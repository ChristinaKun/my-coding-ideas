﻿Hello and thank you for visiting my Git repository!

Here you can see examples of my most recent work as a frontend developer. 
The project is a website named "Freelance Accountant" and its aim is to provide accounting services for self-employed people.
It is composed of two main sections - "Public" and "Admin" with differing access restrictions.

The "Public" section is client-oriented, containing the following features:
- General information about "Freelance Accountant";
- User guide;
- Terms and conditions;
- Contact information;
- User login/logout and registration;
- Declaration form for monthly compensations;
- User profile;
- Declarations from previous months, printable;

http://68.183.208.205:8088/public/index.html

The "Admin" section of the site is accessible only to the administrator/accountant. It allows them to manage the following data:
- List of active users and their declarations;
- Calendar, on which the administrator can mark the holidays in each month;
- Table of insurance types and values (to be filled out every month);
- Table of minimal wages (to be filled out every month);

http://68.183.208.205:8088/admin_login.html

This is still a work in progress, with features such as declaration export, online payment form, etc., to be added in the near future.

The project is based on HTML, CSS, JavaSrcipt, Jquery, Jquery UI.
Design, layout and photo retouch are also made by me.
