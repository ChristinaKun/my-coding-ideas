function loadData(METHOD, URL, onSuccess, onFail) {
	sendRequest(METHOD, URL, null, onSuccess, onFail);
}

function redirect(METHOD, URL, myData, wrongInput, onFail) {
	sendRequest(METHOD, URL, myData, function (response) {
		if (response.code) {
			fadeOut();
			setTimeout(function() {
				window.location = response.code;
			}, 505);
		} else {
			wrongInput();
		}
	}, onFail);
}

function sendRequest(method, url, data, onSuccess, onFail) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			if (this.status >= 200 && this.status < 300) {
				onSuccess(this.response && JSON.parse(this.response));
			} else {
				onFail();
			}
		}
	};
	xhttp.open(method, url, true);
	xhttp.setRequestHeader("Content-type", "application/json");
	xhttp.send(data && JSON.stringify(data));
}

function onSuccess()  { alert("Изпратено успешно!"); }
function onFail()     { alert("Няма достъп до сървъра!"); }
function wrongInput() { alert("Грешен потребител или парола!"); }