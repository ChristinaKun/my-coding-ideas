//Fade in & Fade out
function fadeIn(background, form){
  background.fadeIn("slow");
  form.fadeIn("slow");
}

//Проверка за коректно вписани данни:
function checkUserInput(ev){
  ev.preventDefault();
  var inputNodes = document.querySelectorAll(".obligatory");
  for(let i=0; i<inputNodes.length; i++){
    if(inputNodes[i].value === ""){
      document.getElementById("error").innerHTML = "*Моля, попълнете всички необходими полета!";
      inputNodes[i].classList.add("unchecked");
      return false;
    }
  }
} 

//Изчистване на червените полета след корегиране на некоректно вписани данни:
function clearRedFields(ev){
  $(ev.target).removeClass("unchecked");
  $("#error").text("");
}

//Скриване на формата:
function closeForm(background, form){
  background.fadeOut("slow", function(){background.remove()});
  form.fadeOut("slow", function(){form.remove()});
}