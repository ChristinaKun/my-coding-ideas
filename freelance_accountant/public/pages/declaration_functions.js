//Създаване на декларация:
function openDeclarationForm(){
	//Преброява колко дни има в текущия месец:
	var days = function() {
		var d = new Date();
		return new Date(d.getFullYear(), d.getMonth() + 1, 0).getDate();
	}();

	var background = $("<div>").addClass("background");
	var form = $("<form>").addClass("register");
	$("body").append(background, form);
	var sp  = $("<span>").attr("id", "close").text("X").click(function(){
		closeForm(background, form);
	})
	var p1 = $("<p>").addClass("close").append(sp);
	var h = $("<h2>").addClass("close").text("Социални осигуровки");
	var lb1 = $("<label>").text("Име:");
	var inp1 = $("<input>").attr({"type": "text", "id": "firstName"}).addClass("inputs obligatory");
	var lb2 = $("<label>").text("Презиме:");
	var inp2 = $("<input>").attr({"type": "text", "id": "middleName"}).addClass("inputs obligatory");
	var lb3 = $("<label>").text("Фамилия:");
	var inp3 = $("<input>").attr({"type": "text", "id": "lastName"}).addClass("inputs obligatory");
	var lb4 = $("<label>").text("Дата на раждане:");
	var inp4 = $("<input>").attr({"type": "text", "id": "dateOfBirth"}).addClass("inputs obligatory");
	var lb5 = $("<label>").text("ЕГН:");
	var inp5 = $("<input>").attr({"type": "text", "id": "personalNumber"}).addClass("inputs obligatory");
	var lb6 = $("<label>").text("Здравноосигурен съм:");
	var check = $("<input>").attr({"type": "checkbox"}).addClass("checkbox");
	var lb7 = $("<label>").text("Вид здравноосигурен:");
	var pick = $("<select>").addClass("inputs");
	pick.required = 'true';
	var opt1 = $("<option>").text("стандартен").val('REGULAR');
	var opt2 = $("<option>").text("инвалид").val('DISABLED');
	var opt3 = $("<option>").text("пенсионер").val('RETIRED');
	var opt4 = $("<option>").text("пенсионер-инвалид").val('DISABLED_RETIRED');
	pick.append(opt1, opt2, opt3, opt4);

	var lb8 = $("<label>").text("Основна работна заплата:");
	var inp8 = $("<input>").attr({"type": "number", "id": "baseSalary"}).addClass("inputs obligatory");
	var lb9 = $("<label>").text("Дни в болнични:");
	var inp9 = $("<input>").attr({"type": "number", "id": "daysSickLeave"}).addClass("inputs").val(0);
	var lb10 = $("<label>").text("Дни в майчинство:");
	var inp10 = $("<input>").attr({"type": "number", "id": "daysMaternityLeave"}).addClass("inputs").val(0);
	var lb11 = $("<label>").text("Ден на стартиране на дейността:");
	var inp11 = $("<input>").addClass("inputs")
		.attr({"type": "number", "id": "activityStartDay", "min": 1, "max": days});
	var lb12 = $("<label>").text("Ден на край на дейността:");
	var inp12 = $("<input>").addClass("inputs")
		.attr({"type": "number", "id": "activityEndDay", "min": 1, "max": days});

	var p2 = $("<p>").attr("id", "error");
	var btn = $("<button>").text("Изпрати!").attr("type", "submit");

	form.append(p1, h, lb1, inp1, lb2, inp2, lb3, inp3, lb4, inp4, lb5, inp5, lb6, check, lb7, pick);
	form.append(lb8, inp8, lb9, inp9, lb10, inp10, lb11, inp11, lb12, inp12, p2, btn);

	$( function(){ 
		$("#dateOfBirth").datepicker();
		var changeYear = $("#dateOfBirth").datepicker( "option", "changeYear" );
		$("#dateOfBirth").datepicker( "option", "changeYear", true );
		var yearRange = $("#dateOfBirth").datepicker( "option", "yearRange" );
		$("#dateOfBirth").datepicker( "option", "yearRange", "1930:" + new Date().getFullYear );
		var dateFormat = $("#dateOfBirth").datepicker( "option", "dateFormat" );
		$("#dateOfBirth").datepicker( "option", "dateFormat", "yy-mm-dd" );
	} );
	$("input").click(clearRedFields);
	$("button").click(function(ev){
			checkUserInput(ev);
		})
		.click(function(ev){
			postInsuranceDeclaration(background, form);
		});

	fadeIn(background, form);
	loadData("GET", "/api/users/compensations", loadDeclarationData, onFail);
}

function convertDeclarationData(){
	var insInput = {firstName:"", middleName:"", lastName:"", dateOfBirth:"", personalNumber:"", hasHealthInsurance:"", insuranceType:"", baseSalary:"", daysSickLeave:"", daysMaternityLeave:"", activityStartDay:"", activityEndDay:""};
	var inputNodes = document.querySelectorAll(".register>input, .register>select");
	var obligatoryInput = document.querySelectorAll(".obligatory");
	for(let i=0; i<obligatoryInput.length; i++){
		if(obligatoryInput[i].value === ""){
			return 0;
		}
	}
	insInput.firstName = inputNodes[0].value;
	insInput.middleName = inputNodes[1].value;
	insInput.lastName = inputNodes[2].value;
	var d = new Date(inputNodes[3].value);
	insInput.dateOfBirth = `${d.getMonth() + 1}/${d.getDate()}/${d.getFullYear()}`;
	insInput.personalNumber = inputNodes[4].value;
	insInput.hasHealthInsurance = inputNodes[5].checked; 
	insInput.insuranceType = inputNodes[6].value;
	insInput.baseSalary = inputNodes[7].value;
	insInput.daysSickLeave = inputNodes[8].value;
	insInput.daysMaternityLeave = inputNodes[9].value;
	insInput.activityStartDay = inputNodes[10].value;
	insInput.activityEndDay = inputNodes[11].value;
 
	return insInput;
}

function loadDeclarationData(txt){
	var inputNodes = document.querySelectorAll(".register>input, .register>select");
	inputNodes[0].value = txt.firstName;
	inputNodes[1].value = txt.middleName;
	inputNodes[2].value = txt.lastName;
	var dobDate = new Date(txt.dateOfBirth);
	var day = ("0" + dobDate.getDate()).slice(-2);
	var month = ("0" + (dobDate.getMonth() + 1)).slice(-2);
	var dob = dobDate.getFullYear() + "-" + month + "-" + day;
	inputNodes[3].value = dob;
	//трансформиране на датата:
	inputNodes[4].value = txt.personalNumber;
	inputNodes[5].checked = txt.hasHealthInsurance;
	inputNodes[6].value = txt.insuranceType;
	inputNodes[7].value = txt.baseSalary;
	inputNodes[8].value = txt.daysSickLeave;
	inputNodes[9].value = txt.daysMaternityLeave;
	inputNodes[10].value = txt.activityStartDay;
	inputNodes[11].value = txt.activityEndDay;
}

function postInsuranceDeclaration(background, form){
	var myData = convertDeclarationData();
	if(myData){
		sendRequest("POST", "/api/users/compensations", myData, function(){closeForm(background, form)}, onFail);
	}
}