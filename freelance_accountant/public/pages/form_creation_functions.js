//Извикване на формата:
var ANCHORS = document.querySelectorAll('.anchor');
ANCHORS[0].addEventListener('click', displayEntryForm);
ANCHORS[1].addEventListener('click', displayRegForm);

//Форма за вход на регистриран потребител:
function displayEntryForm(ev){	
	var background = $("<div>").addClass("background");
	var form = $("<form>").addClass("register");
	$("body").append(background, form);
	var sp  = $("<span>").attr("id", "close").text("X").click(function(){closeForm(background, form)});
	var p1 = $("<p>").addClass("close").append(sp);
	var h = $("<h2>").addClass("close").text("Вход");
	var lb1 = $("<label>").text("Потребителско име:");
    var inp1 = $("<input>").attr({"type": "text", "id": "username"}).val("Faraonkata").addClass("inputs obligatory");
    var lb2 = $("<label>").text("Парола:");
    var inp2 = $("<input>").attr({"type": "password", "id": "password"}).val("000000").addClass("inputs obligatory");
    var lb3 = $("<label>").text("Приемам бисквитки!");
    var check = $("<input>").attr({"type": "checkbox"}).addClass("checkbox");
    var p2 = $("<p>").attr("id", "error");
    var btn = $("<button>").text("Вход!").attr("type", "submit");

	form.append(p1, h, lb1, inp1, lb2, inp2, lb3, check, p2, btn);

	$("input").click(function(ev){
          clearRedFields(ev);
        });
	$("button").click(function(ev){
	    checkUserInput(ev);
	  })
	  .click(function(ev){
	    postEntryInput(background, form);
	  });

	fadeIn(background, form);
}
//Форма за регистрация на нов потребител:
function displayRegForm(ev){
	var background = $("<div>").addClass("background");
	var form = $("<form>").addClass("register");
	$("body").append(background, form);
	var sp  = $("<span>").attr("id", "close").text("X").click(function(){closeForm(background, form)});
	var p1 = $("<p>").addClass("close").append(sp);
	var h = $("<h2>").addClass("close").text("Регистрация");
	var lb1 = $("<label>").text("Потребителско име:");
	var inp1 = $("<input>").attr({"type": "text", "id": "username"}).addClass("inputs obligatory");
	var lb2 = $("<label>").text("e-mail:");
	var inp2 = $("<input>").attr({"type": "email", "id": "email"}).addClass("inputs obligatory");
	var lb3 = $("<label>").text("Парола:");
	var inp3 = $("<input>").attr({"type": "password", "id": "password"}).addClass("inputs obligatory");
	var p2 = $("<p>").attr("id", "error");
    var btn = $("<button>").text("Регистрирай!").attr("type", "submit");

	form.append(p1, h, lb1, inp1, lb2, inp2, lb3, inp3, p2, btn);

	$("input").click(function(ev){
          clearRedFields(ev);
        });
	$("button").click(function(ev){
	    checkUserInput(ev);
	  })
	  .click(function(ev){
	    postRegInput();
	  });
	fadeIn(background, form);
}

//При влизане в профила:
function logIn(data){
	var hidden = document.querySelector(".hide_me");
	hidden.classList.remove("hide_me");
	hidden.classList.add("show_me");
	document.getElementById("placeholder").classList.add("hide_me");
	changeIcons(data.username);
}

function changeIcons(username){
	var icons = document.querySelectorAll(".anchor>.fas, .anchor>.far");
	icons[0].classList.remove("fa-sign-in-alt");
	icons[0].classList.add("fa-user-tie");
	icons[1].classList.remove("fa-address-book");
	icons[1].classList.remove("far");
	icons[1].classList.add("fas");
	icons[1].classList.add("fa-sign-out-alt");
	ANCHORS[0].removeEventListener('click', displayEntryForm);
	ANCHORS[0].addEventListener('click', displayUserInfo);
	ANCHORS[1].removeEventListener('click', displayRegForm);
	ANCHORS[1].addEventListener('click', logOut);

	document.querySelector("#anchor1>.tooltip").innerText = username;
	document.querySelector("#anchor2>.tooltip").innerText = "Изход";
}

//При излизане от профила:
function logOut(){
	sendRequest("DELETE", "/api/users", null, function () {
		var hidden = document.querySelector(".show_me");
		hidden.classList.remove("show_me");
		hidden.classList.add("hide_me");
		document.getElementById("placeholder").classList.remove("hide_me");
		returnIcons();
	}, onFail);
}

function returnIcons(){
	var icons = document.querySelectorAll(".anchor>.fas, .anchor>.far");
	icons[0].classList.remove("fa-user-tie");
	icons[0].classList.add("fa-sign-in-alt");
	icons[1].classList.remove("fa-sign-out-alt");
	icons[1].classList.remove("fas");
	icons[1].classList.add("far");
	icons[1].classList.add("fa-address-book");
	ANCHORS[0].addEventListener('click', displayEntryForm);
	ANCHORS[0].removeEventListener('click', displayUserInfo);
	ANCHORS[1].addEventListener('click', displayRegForm);
	ANCHORS[1].removeEventListener('click', logOut);

	document.querySelector("#anchor1>.tooltip").innerText = "Вход";
	document.querySelector("#anchor2>.tooltip").innerText = "Регистрация";
}

//Конвертиране на данни от форма "Вход":
function convertEntryData(){
	var inputNodes = document.querySelectorAll(".register>input, .register>textarea");
	var entryInput = {username:"", password:"", acceptCookies:""};

	entryInput.username = inputNodes[0].value;
	entryInput.password = inputNodes[1].value;
	entryInput.acceptCookies = inputNodes[2].checked;

	for(let i=0; i<inputNodes.length; i++){
		if(inputNodes[i].value === ""){
			return 0;
		}
	}
	if(!entryInput.acceptCookies){
		document.getElementById("error").innerText = "*Моля, разрешете ползването на бисквитки!";
		return 0;
	}
	return entryInput;
}

function displayRegSuccessMessage(){
	$(".register>input, .register>label, .register>button").css("display", "none");
	$("#error").text("*Регистрацията е успешна! Ще получите линк за активация на посочения имейл.");
}

function convertRegData(){
	var username = $("#username").val();
	var email = $("#email").val();
	var password = $("#password").val();
	if (username && email && password) {
		var regInput = { "username": username, "email": email, "password": password };
		return regInput;
	}
}

//Заявка към сървър:
function postEntryInput(background, form) {
	var myData = convertEntryData();
	if(myData){
		sendRequest("PUT", "/api/users", myData, function () { onSuccessfulLogin(myData, background, form); }, onFail);
	}
}

function onSuccessfulLogin(data, background, form) {
	closeForm(background, form);
	logIn(data);
}

function postRegInput() {
	var myData = convertRegData();
	if(myData){
		sendRequest("POST", "/api/users", myData, displayRegSuccessMessage, onFail);
	}
}

function displayUserInfo() {
	var background = $("<div>").addClass("background");
	var form = $("<div>").addClass("users");
	var sp  = $("<span>").attr("id", "close").text("X").click(function(){closeForm(background, form)});
	var p1 = $("<p>").addClass("close").append(sp);
	var h = $("<h2>").addClass("close").text("Справки:");
	var wrapper  = $("<div>").attr("id", "table_wrapper")
	var table = $("<table>");
	var header = $("<tr><th>Дата</th><th>Ведомост</th><th>Фиш</th><th>Рекапитулация</th><th>Обр. 1</th><th>Обр. 6</th></tr>");
	sendRequest("GET", "/api/users/profiles", null, function (dates) {
		for (var i = 0; i < dates.length; ++i) {
			table.append(displayDeclarations(dates[i]));
		}
	}, onFail);
	table.append(header);
	wrapper.append(table);
	form.append(p1, h, wrapper);
	$("body").append(background, form);
	fadeIn(background, form);
}

function displayDeclarations(monthYear) {
	var month = $("<td>").text(monthYear);
	var payroll = $("<td>").text("Преглед").addClass("view").click(function () {
		loadData("GET", "/api/users/inqueries/payrolls?monthYear=" + monthYear, displayPayroll, onFail);
	});
	var payslip = $("<td>").text("Преглед").addClass("view").click(function () {
		loadData("GET", "/api/users/inqueries/payrolls?monthYear=" + monthYear, displayPayslip, onFail);
	});
	var recap = $("<td>").text("Преглед").addClass("view").click(function () {
		loadData("GET", "/api/users/inqueries/payrolls?monthYear=" + monthYear, displayRecap, onFail);
	});
	var download = $("<a>").attr("href", "/api/users/forms/one?monthYear=" + monthYear).attr("download", "EMPL2019.txt").text("Сваляне");
	var obr1 = $("<td>").append(download).addClass("view");
	var obr6 = $("<td>").text("Сваляне").addClass("view").click(function () { alert("Clicked"); });

	return $("<tr>").append(month, payroll, payslip, recap, obr1, obr6);
}

function displayRecap(recap){
	$(".users").hide();
	var container = $("<div>").addClass("container");
	var to_print = $("<div>").addClass("money_background").attr("id", "print_me")
		.css({	"width": "100%", "text-align": "left", "vertical-align": "middle"});

	//Header
	appendHeader(container, "Рекапитулация:");

	var tableIfo = ["Отдел",
	"Данъци и осигуровки - РЕКАПИТУЛАЦИЯ",
	"ставка",
	"база",
	"сума",
	"0",
	"Вноски за сметка на работодател",
	"",
	"",
	"Всичко:",
	"0.00",
	"",
	"   ",
	"Лични вноски на персонала",
	"",
	"ДОО и всички осиг. случай - собственик / след 1960 г",
	recap.dooPercent,
	recap.baseSalary,
	recap.dooAmount,
	"551111",
	"ДЗПО-УПФ или доп.вн. за ф.Пенсии - собственици род. след",
	recap.dzpoPercent,
	recap.baseSalary,
	recap.dzpoAmount,
	"581111",
	"Здравна осиг. - собственик",
	recap.zoPercent,
	recap.baseSalary,
	recap.zoAmount,
	"561111",
	"",
	"Всичко:",
	recap.totalDeduction,
	"",
	"",
	"",
	"Счетоводител:",
	"",
	"Управител",
	""
	];

	var table = $("<table>").css({"margin-left": "auto", "margin-right": "auto", "border": "1px solid transparent", "border-collapse": "collapse", "font-family": "'Arial Narrow', sans-serif", "text-align": "left"});
	var caption = $("<caption>").text("Рекапитулация за месец " + recap.monthYear).css({"height": "1.8rem", "margin": "0", "font-size": "1.2rem", "line-height": "1.8rem", "font-weight": "bold", "text-align": "center", "vertical-align": "middle", "background-color": "#FFF", "border-bottom": "2px solid #147", "border-collapse": "collapse"});

	var tr1 = $("<tr>")
		.append($("<td>").attr("colspan", 4).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}),
			$("<td>").text(tableIfo[0]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem", "font-weight": "bold"}));
	var tr2 = $("<tr>")
		.append($("<td>").text(tableIfo[1]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem", "font-weight": "bold", "border-bottom": "1px solid #000", "border-collapse": "collapse",}),
			$("<td>").text(tableIfo[2]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}),
			$("<td>").text(tableIfo[3]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}),
			$("<td>").text(tableIfo[4]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}),
			$("<td>").text(tableIfo[5]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));
	var tr3 = $("<tr>")
		.append($("<td>").text(tableIfo[6]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem", "border-bottom": "1px solid #000", "border-collapse": "collapse"}),
			$("<td>").text(tableIfo[7]).attr("colspan", 4).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}),);
	var tr4 = $("<tr>")
		.append($("<td>").text(tableIfo[8]).attr("colspan", 2).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}),
			$("<td>").text(tableIfo[9]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem", "font-weight": "bold"}),
			$("<td>").text(tableIfo[10]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem", "border-top": "1px solid #000", "border-bottom": "1px double #000", "border-collapse": "collapse"}),
			$("<td>").text(tableIfo[11]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));
	var tr5 = $("<tr>")
		.append($("<td>").text(tableIfo[12]).attr("colspan", 5).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));
	var tr6 = $("<tr>")
		.append($("<td>").text(tableIfo[13]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem", "border-bottom": "1px solid #000", "border-collapse": "collapse"}),
			$("<td>").text(tableIfo[14]).attr("colspan", 4).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));
	var tr7 = $("<tr>");
	var tr8 = $("<tr>");
	var tr9 = $("<tr>");
	for(var i=15; i<20; i++){
		tr7.append($("<td>").text(tableIfo[i]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));
		tr8.append($("<td>").text(tableIfo[i+5]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));
		tr9.append($("<td>").text(tableIfo[i+10]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));
	}
	var tr10 = $("<tr>")
		.append($("<td>").text(tableIfo[30]).attr("colspan", 2).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}),
			$("<td>").text(tableIfo[31]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem", "font-weight": "bold"}),
			$("<td>").text(tableIfo[32]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem", "border-top": "1px solid #000", "border-bottom": "1px double #000", "border-collapse": "collapse"}),
			$("<td>").text(tableIfo[33]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));;
	var tr11 = $("<tr>")
		.append($("<td>").text(tableIfo[34]).attr("colspan", 5).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));
	var tr12 = $("<tr>");
	for(var i=35; i<40; i++){
		tr12.append($("<td>").text(tableIfo[i]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"}));
	}

	table.append(caption, tr1, tr2, tr3, tr4, tr5, tr6, tr7, tr8, tr9, tr10, tr11, tr12);
	to_print.append(table);
	container.append(to_print);
	//Footer
	appendFooter(container);
	 
 	runEffect(container, null);
}

function displayPayslip(payslip){
	$(".users").hide();
	var container = $("<div>").addClass("container");
	var to_print = $("<div>").addClass("money_background").attr("id", "print_me")
		.css({	"width": "100%", "text-align": "left", "vertical-align": "middle"});

	//Header
	appendHeader(container, "Фиш:");

	var tableIfo = [
		payslip.fullName,
		payslip.yearOfBirth,
		"Вид осигурен",
		"12",
		"Собственик или земеделски производител - осигурен за пенсия, здравно и социален риск",
		"Възнаграждение за месец:",
		payslip.monthYear,
		"Работни дни:",
		payslip.daysWorking,
		"Трудов стаж гг/мм/дд",
		"00г 00м 00д",
		"% за труд. възн. за стаж",
		"0.00%",
		"Длъжност",
		"Самоосигуряващ се",
		"", "",
		"Основна работна заплата",
		payslip.baseSalary,
		"Осигурителен доход",
		payslip.salary,
		"Отработени дни за месеца",
		payslip.daysWorking,
		"Данъчна основа",
		"",
		"Дневно работно време /ч./",
		"",
		"Минимален осиг. доход",
		payslip.minSalary,
		"НАЧИСЛЕНИЯ /дни/",
		"",
		"УДРЪЖКИ",
		"",
		"Възнаграждение за отработени дни" + "/" + payslip.daysWorking + "/",
		"?",
		"Фондове ДОО " + payslip.dooPercent + "%",
		payslip.dooAmount,
		"Платен отпуск",
		"",
		"Ф. Пенсии в/у соц. р/ди",
		"0.00",
		"Социални разходи",
		"",
		"ДЗПО-УПФ или Ф.Пенси " + payslip.dzpoPercent + "%",
		payslip.dzpoAmount,
		"Трудово възнагражд. за стаж",
		"",
		"Здр. осиг. вноска " + payslip.zoPercent + "%",
		payslip.zoAmount,
		"Трудово възнагражд. за болнични",
		"",
		"Неплатен отпуск",
		"",
		"Болнични и бременност",
		payslip.daysSick,
		"ДОД по чл.42 ЗДДФЛ",
		"0.00",
		"Отглеждане на дете до 2 год.",
		payslip.daysMaternity,
		"Данъчни облекчения",
		"0.00",
		"Други възнаграждения облагаеми",
		"",
		"", "", "", "",
		"Месечен аванс",
		"",
		"", "",
		"Доброволни удръжки",
		"",
		"Други необлагаеми",
		"",
		"", "", "", "", "", "",
		"Суми, в/у които се дължи ДОД",
		"0.00",
		"", "",
		"Всичко начисления",
		"?",
		"Всичко удръжки",
		payslip.totalDeduction,
		" ", " ", " ", " ",
		"Изплатени в брой",
		payslip.salary - payslip.totalDeduction,
		"Подпис",
		"............"];

	var table = $("<table>").css({"margin-left": "auto", "margin-right": "auto", "border": "1px solid transparent", "border-collapse": "collapse", "font-family": "'Arial Narrow', sans-serif", "text-align": "left"});
	var caption = $("<caption>").text("Ведомост за заплати:").css({"height": "1.8rem", "margin": "0", "font-size": "1.2rem", "line-height": "1.8rem", "font-weight": "bold", "text-align": "center", "vertical-align": "middle", "background-color": "#FFF", "border-bottom": "2px solid #147", "border-collapse": "collapse"});
	table.append(caption);

	var tr1 = $("<tr>").css({"font-weight": "bold"});
	for(var i=0; i<4; i++){
		var td1 = $("<td>").text(tableIfo[i]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"});
		tr1.append(td1);
	}
	table.append(tr1);

	var tr2 = $("<tr>").css({"font-weight": "bold"});
	var td2 = $("<td>").text(tableIfo[4]).attr("colspan", 4)
		.css({"color": "red", "background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"});
	tr2.append(td2);
	table.append(tr2);

	var tr3 = $("<tr>").css({"font-weight": "bold"}); 
	for(var i=5; i<9; i++){
		var td3 = $("<td>").text(tableIfo[i]).css({"height": "4rem", "background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"});
		tr3.append(td3);
	}
	table.append(tr3);
	
	for(var j=0; j<5; j++){
		var tr4 = $("<tr>");
		for(var i = 4*j + 9; i < 4*j + 13; i++){
			var td4 = $("<td>").text(tableIfo[i]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"});
			tr4.append(td4);
		}
		table.append(tr4);
	}

	var tr5 = $("<tr>").css({"font-weight": "bold", "border-top": "2px solid #147", "border-bottom": "2px solid #147", "border-collapse": "collapse"});
	for(var i = 29; i < 33; i++){
		var td5 = $("<td>").text(tableIfo[i]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"});
		tr5.append(td5);
	}
	table.append(tr5);

	for(var k=0; k<13; k++){
		var tr6 = $("<tr>");
		for(var i = 4*k + 33; i < 4*k + 37; i++){
			var td6 = $("<td>").text(tableIfo[i]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"});
			if(i == 59 || i == 60) {
				td6.css({"border-top": "2px solid #147", "border-bottom": "2px solid #147", "border-collapse": "collapse", "font-style": "italic"});
			}
			if(i == 33){td6.css("color", "red");}
			tr6.append(td6);
		}
		table.append(tr6);
	}
	
	for(var l=0; l<3; l++){
		var tr7 = $("<tr>").css({"font-weight": "bold", "border-top": "2px solid #147", "border-bottom": "2px solid #147", "border-collapse": "collapse"});
		for(var i = 4*l + 85; i < 4*l + 89; i++){
			var td7 = $("<td>").text(tableIfo[i]).css({"background-color": "#FFF", "padding": "0.25rem", "min-width": "6rem"});
			if(i == 85){td7.css("color", "red");}
			tr7.append(td7);
		}
		table.append(tr7);
	}

	to_print.append(table);
	container.append(to_print);
	//Footer
	appendFooter(container);

	runEffect(container, null);
}

function displayPayroll(payroll) {
	$(".users").hide();
	var container = $("<div>").addClass("container");
	var to_print = $("<div>").addClass("money_background").attr("id", "print_me")
		.css({	"width": "100%", "text-align": "left", "vertical-align": "middle"});

	//Header
	appendHeader(container, "Ведомост:");

	//Основна част на таблицата:
	var tableIfo = ["№", 
		"Име, Презиме, Фамилия",
		"Вид осигурен",
		"Година на раждане",
		"Длъжност",
		"Отдел",
		"Трудов стаж години",
		"гг/мм/дд",
		"Минимален осигурителен доход",
		"Основна работна заплата",
		"Отработени дни за месеца",
		"Дневно работно време /часа/",
		"Основно възнаграждение",
		"Дни в неплатен отпуск",
		"Редовен и допълнителен годишен отпуск",
		"дни",
		"сума",
		"Социални разходи, в/у които се дължат осигуровки",
		"% за доп. труд. възн. за придобит труд. стаж и проф. опит /за година/",
		"Доп. трудови възнаграждения за придобит трудов стаж и проф. опит",
		"Трудово възнаграждение за болнични от работодател",
		"дни",
		"сума",
		"Допълнителни възнаграждения облагаеми",
		"ВСИЧКО ТРУДОВО ВЪЗНАГРАЖДЕНИЕ",
		"№",
		"Дни в болнични за с/ка на ДОО",
		"Дни в отгл.на дете за с/ка на ДОО",
		"Други необлагаеми трудови възнаграждения",
		"БРУТНО ТРУДОВО ВЪЗНАГРАЖДЕНИЕ",
		"Осигурителен доход",
		"Фондове ДОО " + payroll.dooPercent + "%",
		"Фондове ДОО в/у социaлни разходи 0%",
		"Лице без здравно осигуряване",
		"Здравно осигуряване " + payroll.zoPercent + "%",
		"ДЗПО - УПФ или допълн. вноска за ф. Пенсии " + payroll.dzpoPercent + "%",
		"Данъчни облекчения",
		"Суми, върху които се дължи само ДОД",
		"СУМА ЗА ОБЛАГАНЕ С ДОД",
		"ДОД",
		"Доброволни удръжки",
		"Общо удръжки",
		"Чиста сума за получаване",
		"Авансово плащане",
		"Подпис за получен аванс",
		"Остатък за получаване",
		"Подпис"];

	var userInfo = ["1",
		payroll.fullName,
		"12",
		payroll.yearOfBirth,
		"Самоосигуряващ се",
		"",
		"00г 00м 00д",
		payroll.minSalary,
		payroll.baseSalary,
		payroll.daysWorking,
		"",
		payroll.salary,
		"",
		"",
		"",
		"",
		"",
		"0.00",
		"",
		"",
		"",
		 payroll.salary,
		payroll.daysSick,
		payroll.daysMaternity,
		"",
		payroll.salary,
		payroll.salary,
		payroll.dooAmount,
		"0.00",
		payroll.hasHealthInsurance ? "не" : "да",
		payroll.zoAmount,
		payroll.dzpoAmount,
		"",
		"",
		"0.00",
		"0.00",
		"",
		payroll.totalDeduction,
		"0.00",
		"",
		"",
		"0.00",
		""];	

	//Табица - Първа половина:
	var table = $("<table>").css({"margin-left": "auto", "margin-right": "auto", "border": "1px solid #000", "border-collapse": "collapse", "font-family": "'Arial Narrow', sans-serif", "text-align": "left", "font-size": "14px"});
	var caption = $("<caption>").text("РАЗЧЕТНО ПЛАТЕЖНА ВЕДОМОСТ:").css({"margin": "0", "font-size": "16px", "font-weight": "bold", "text-align": "center", "background-color": "#FFF", "border-bottom": "1.5px solid #147", "border-collapse": "collapse"});

	var tr1 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});
	var tr2 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});
	var tr3 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});
	var tr4 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});
	var sumroll1 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});
	var tr5 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse", "height": "25px"});
	var tr6 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});
	var tr7 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});
	var tr8 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});
	var tr9 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});
	var sumroll2 = $("<tr>").css({"border": "1px solid #000", "border-collapse": "collapse"});

	table.append(caption, tr1, tr2, tr3, tr4, sumroll1, tr5, tr6, tr7, tr8, tr9, sumroll2);

	for(var i=0; i<6; i++){
		var td1 = $("<td>").attr("rowspan", "2")
			.css({"background-color": "#FFF", "padding": "3px", "border": "1px solid #000", "border-collapse": "collapse"})
			.append($("<span>").text(tableIfo[i]).css({"writing-mode": "vertical-rl", "display": "block", "height": "220px"}));
		tr1.append(td1);
	}
	
	tr1.append($("<td>")
		.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"})
		.append($("<span>").text(tableIfo[6]).css({"writing-mode": "vertical-rl", "display": "block", "height": "195px"})));
	tr2.append($("<td>").text(tableIfo[7]).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));

	for(var i=8; i<14; i++){
		var td2 = $("<td>").attr("rowspan", "2")
			.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"})
			.append($("<span>").text(tableIfo[i]).css({"writing-mode": "vertical-rl", "display": "block", "height": "220px"}));
		tr1.append(td2);
	}

	tr1.append($("<td>").attr("colspan", "2")
		.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"})
		.append($("<span>").text(tableIfo[14]).css({"writing-mode": "vertical-rl", "display": "block", "height": "195px"})));
	tr2.append($("<td>").text(tableIfo[15]).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	tr2.append($("<td>").text(tableIfo[16]).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));

	for(var i=17; i<20; i++){
		var td3 = $("<td>").attr("rowspan", "2")
			.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"})
			.append($("<span>").text(tableIfo[i]).css({"writing-mode": "vertical-rl", "display": "block", "height": "220px"}));
		tr1.append(td3);
	}

	tr1.append($("<td>").attr("colspan", "2")
		.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"})
		.append($("<span>").text(tableIfo[20]).css({"writing-mode": "vertical-rl", "display": "block", "height": "195px"})));
	tr2.append($("<td>").text(tableIfo[21]).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	tr2.append($("<td>").text(tableIfo[22]).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));

	for(var i=23; i<25; i++){
		var td4 = $("<td>").attr("rowspan", "2")
			.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"})
			.append($("<span>").text(tableIfo[i]).css({"writing-mode": "vertical-rl", "display": "block", "height": "220px"}));
		tr1.append(td4);
	}

	for(var j=0; j<22; j++){
		tr3.append($("<td>").text(j+1).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
		tr4.append($("<td>").text(userInfo[j]).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	}

	sumroll1.append($("<td>").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("Общо").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse", "text-align": "right"}));
	for(var z=0; z<9; z++){
		sumroll1.append($("<td>").text("-").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	}
	sumroll1.append($("<td>").text(payroll.salary).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("-").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("-").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("-").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("-").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll1.append($("<td>").text(payroll.salary).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));

	tr5.append($("<td>").attr("colspan", "11").text("Работни дни: " + " " + payroll.daysWorking)
		.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "height": "45px", "text-align": "center", "border-bottom": "1px solid #000", "border-collapse": "collapse"}));
	tr5.append($("<td>").attr("colspan", "11").text("месец: " + payroll.monthYear)
		.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "height": "45px", "text-align": "center", "border-bottom": "1px solid #000", "border-collapse": "collapse"}));

	//Таблица - Втора половина:
	for(var i=25; i<31; i++){
		var td5 = $("<td>").attr("rowspan", "2")
			.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"})
			.append($("<span>").text(tableIfo[i]).css({"writing-mode": "vertical-rl", "display": "block", "height": "220px"}));
		tr6.append(td5);
	}

	tr6.append($("<td>").text("ЛИЧНИ ОСИГУРОВКИ").attr("colspan", "5")
		.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	for(var i=31; i<36; i++){
		tr7.append($("<td>")
			.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"})
			.append($("<span>").text(tableIfo[i]).css({"writing-mode": "vertical-rl", "display": "block", "height": "195px"})));
	}

	for(var i=36; i<47; i++){
		var td6 = $("<td>").attr("rowspan", "2")
			.css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"})
			.append($("<span>").text(tableIfo[i]).css({"writing-mode": "vertical-rl", "display": "block", "height": "220px"}));
		tr6.append(td6);
	}

	tr8.append($("<td>").text(1).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	tr9.append($("<td>").text(1).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));

	for(var k=22; k<43; k++){
		tr8.append($("<td>").text(k+1).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
		tr9.append($("<td>").text(userInfo[k]).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	}

	sumroll2.append($("<td>").text("-").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").text("-").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	for(var y=25; y<29; y++){
		sumroll2.append($("<td>").text(userInfo[y]).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	}
	sumroll2.append($("<td>").text("-").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").text(payroll.zoAmount).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").text(payroll.dzpoAmount).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	for(var u=0; u<5; u++){
		sumroll2.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));		
	}
	sumroll2.append($("<td>").text(payroll.totalDeduction).css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").text("0.00").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));
	sumroll2.append($("<td>").css({"background-color": "#FFF", "padding": "4px", "min-width": "20px", "border": "1px solid #000", "border-collapse": "collapse"}));

	to_print.append(table);
	container.append(to_print);
	
	//Footer
	appendFooter(container);

	runEffect(container, null);
}

function appendHeader(container, title){
	var close  = $("<span>").attr("id", "close").text("X").click(function(){
		runEffect(container, function(){
			container.remove();
			$(".users").show();
		});
		});
	var print = $("<b>&#9113;</b>").attr("id", "print").click(function(){
		var divToPrint = document.getElementById("print_me");
		newWin= window.open("");
		newWin.document.write(divToPrint.outerHTML);
		newWin.print();
		newWin.close();
	});
	var p1 = $("<p>").addClass("close").append(print, close);
	var h = $("<h2>").text(title);
	var head = $("<div>").addClass("container_header").append(p1, h); 	
	container.append(head);		
}

function appendFooter(container){
	var logo = $("<h3><i class='fas fa-paperclip'></i>&nbsp;СчеТито Ти!</h3>")
		.css({
			"font-family": "'Philosopher', sans-serif",
			"text-align": "center"
		});
	var footer = $("<div>").addClass("container_footer").append(logo);
	container.append(footer);
	$("body").append(container);
}

// Run the effect
function runEffect(container, callback) {
	// get effect type from
	var selectedEffect = "blind";

	// Most effect types need no options passed by default
	var options = {};
	// some effects have required parameters
	if ( selectedEffect === "scale" ) {
		options = { percent: 50 };
	} else if ( selectedEffect === "size" ) {
		options = { to: { width: 280, height: 185 } };
	}
	if(container.css("display") == "none"){
		container.show( selectedEffect, options, 1000 );
	}else{
		container.hide( selectedEffect, options, 1000, callback );
	}
};

loadData("GET", "/api/users", logIn, function () {});
